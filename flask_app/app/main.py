import json
import asyncio

import itertools
import requests
from loguru import logger

from init_app import app
from utils import fetch


@app.route('/')
async def index():
    urls = [
        'http://frontend.localhost/source1.json',
        'http://frontend.localhost/source2.json',
        'http://frontend.localhost/source3.json',
        'http://frontend.localhost/fail.json',
        'http://api.localhost/api/long/10'
    ]
    response = await asyncio.gather(*[fetch(url) for url in urls])
    result = sorted(list(itertools.chain(*response)), key=lambda x: x['id'])
    return json.dumps(result)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=False)
