import httpx
from loguru import logger


async def fetch(url):
    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(url, timeout=2)
            logger.debug(response.status_code)
            return response.json() if response.status_code == 200 else []
    except httpx.ReadTimeout:
        return []
