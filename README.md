# back-end-test-task

#### Software development stack ####
- Python:3.8
- Postgres:12.3-alpine
- FastApi 0.77.1 (API app)
- SQLAlchemy 1.4.36
- Traefik:v2.6

#### URLs ####
http://frontend.localhost (Main page) \
http://api.localhost/api/docs (API documentation) \
http://flask.localhost (Asynchronous requests) \
http://traefik.localhost/dashboard (Traefik bashboard)

#### Build and run ####
Install **docker**: https://docs.docker.com/engine/install/ \
Install **docker-compose**: https://docs.docker.com/compose/install/

> To execute the following commands you need to be in the root of the project

```
docker-compose up --build
```

#### Credentials ####
- **Main page:**\
Login: **admin**\
Password: **admin_password**


- **API documentation:**\
Login: **admin**\
Password: **AdminPassword**