$('#login').click(function() {
  let username = $('#username').val()
  let password = $('#password').val()
  login(username, password)
});

function login(username, password) {
  $.ajax({
    url: "http://api.localhost/auth/login",
    method: "POST",
    timeout: 0,
    headers: {"Content-Type": "application/json"},
    data: JSON.stringify({"username": username, "password": password}),
    success: function(data) {
      let access_token = data.access_token
      localStorage.setItem("access_token", access_token)
      window.location.replace("http://frontend.localhost/users.html");
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
       alert(errorThrown);
    }
  })
}