$(document).ready(function() {
  getUsers();
  $('.modal').modal();
});

$( "#allCheckbox" ).change(function() {
  let state = $("#allCheckbox").prop('checked');
  $('.user_checkbox').each(function(index, td) {
    $(this).prop('checked', state);
  });
});

$('#create-user').click(function() {
  user_data = {
    "username": $('#username').val(),
    "first_name": $('#firstName').val() ? $('#firstName').val() : null,
    "last_name": $('#lastName').val() ? $('#lastName').val() : null,
    "password": $('#password').val(),
    "is_superuser": $('#isSuperuser').prop('checked'),
    "readonly": $('#readonly').prop('checked')
  }
  createUser(user_data);
});

$('#delete-users').click(function() {
  $('.user_checkbox').each(function(idx, td) {
    if($(this).prop('checked')){
      let user_id = $(this).parent().parent().parent().attr('id');
      deleteUsers(user_id);
    }
  });
});

$(document).on('click', '.editUser', function() {
  let user_id = $(this).parent().parent().attr('id');

  let first_name = $(`#${user_id}`).find('td').eq(3).text();
  let last_name = $(`#${user_id}`).find('td').eq(4).text();
  let readonly = $(`#${user_id}`).find('td').eq(5).find('input').prop('checked');
  let is_superuser = $(`#${user_id}`).find('td').eq(6).find('input').prop('checked');
  let is_active = $(`#${user_id}`).find('td').eq(7).find('input').prop('checked');

  $("#editedFirstName").val(first_name);
  $("#editedLastName").val(last_name);
  $("#editedIsActive").prop('checked', is_active);
  $("#editedIsSuperuser").prop('checked', is_superuser);
  $("#editedReadonly").prop('checked', readonly);
  $(".updateUser").attr('id', `edit-${user_id}`);

  $("#modal_edit_user").modal('open');
});

$(document).on('click', '.updateUser', function() {
  let user_id = $(this).attr('id').split("-")[1];
  let user_data = {
    "first_name": $('#editedFirstName').val(),
    "last_name": $('#editedLastName').val(),
    "password": $('#editedPassword').val() ? $('#editedPassword').val() : null,
    "readonly": $('#editedReadonly').prop('checked'),
    "is_superuser": $('#editedIsSuperuser').prop('checked'),
    "is_active": $('#editedIsActive').prop('checked')
  };
  if (!user_data['password']) {
    delete user_data['password']
  }
  updateUser(user_id, user_data);
});

$(document).on('click', '#logout', function() {
   localStorage.removeItem("access_token")
   window.location.replace("http://frontend.localhost/");
});

function getUsers() {
  let access_token = localStorage.getItem("access_token")
  $.ajax({
    url: "http://api.localhost/admin/users",
    method: "GET",
    timeout: 0,
    headers: {
      "Content-Type": "application/json",
      "Authorization": access_token
    },
    success: function(data) {
      $.each(data , function(idx, val) {
        addUserToTable(val);
      });
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
       alert(errorThrown);
    }
  })
}

function addUserToTable(data, prepend = false) {
  let is_superuser = data['is_superuser'] ? `checked="checked"` : ''
  let is_active = data['is_active'] ? `checked="checked"` : ''
  let readonly = data['readonly'] ? `checked="checked"` : ''
  let first_name = data['first_name'] ? data['first_name'] : ''
  let last_name = data['last_name'] ? data['last_name'] : ''

  let html_content = `<tr id="${data['id']}">
    <td>
      <a class="editUser" href="#!"><i class="material-icons">edit</i></a>
    </td>
    <td style="width: 25px; text-align: center">
      <label><input type="checkbox" class="user_checkbox" /><span></span></label>
    </td>
    <td>${data['username']}</td>
    <td>${first_name}</td>
    <td>${last_name}</td>
    <td>
      <label><input type="checkbox" ${readonly} disabled="disabled" /><span></span></label>
    </td>
    <td>
      <label><input type="checkbox" ${is_superuser} disabled="disabled" /><span></span></label>
    </td>
    <td>
      <label><input type="checkbox" ${is_active} disabled="disabled" /><span></span></label>
    </td>
  </tr>`
  if (prepend) {$('#user_list').prepend(html_content)} else {$('#user_list').append(html_content)};
}

function createUser(user_data) {
  let access_token = localStorage.getItem("access_token")
  $.ajax({
    url: "http://api.localhost/admin/user",
    method: "POST",
    timeout: 0,
    headers: {
      "Content-Type": "application/json",
      "Authorization": access_token
    },
    data: JSON.stringify(user_data),
    success: function(data) {
      addUserToTable(data);
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
       alert(errorThrown);
    }
  })
}

function updateUser(user_id, user_data) {
  let access_token = localStorage.getItem("access_token")
  $.ajax({
    url: `http://api.localhost/admin/user/${user_id}`,
    method: "PATCH",
    timeout: 0,
    headers: {
      "Content-Type": "application/json",
      "Authorization": access_token
    },
    data: JSON.stringify(user_data),
    success: function(data) {
      $(`#${user_id}`).remove();
      addUserToTable(data, true);
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
       alert(errorThrown);
    }
  })
}

function deleteUsers(user_id) {
  let access_token = localStorage.getItem("access_token")
  $.ajax({
    url: `http://api.localhost/admin/user/${user_id}`,
    method: "DELETE",
    timeout: 0,
    headers: {
      "Content-Type": "application/json",
      "Authorization": access_token
    },
    success: function(data) {
      $(`#${user_id}`).remove();
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
       alert(errorThrown);
    }
  })
}

