import time

from fastapi.openapi.docs import get_swagger_ui_html
from fastapi.openapi.utils import get_openapi
from fastapi import Depends
from fastapi import APIRouter
from starlette.responses import JSONResponse

from security import basic_auth_validation
import config as cfg
from init_app import app


router = APIRouter()


@router.get("/docs")
async def get_api_docs(current_user: str = Depends(basic_auth_validation)):
    return get_swagger_ui_html(openapi_url="/api/openapi.json", title="docs")


@router.get("/openapi.json")
async def get_api_docs(current_user: str = Depends(basic_auth_validation)):
    return JSONResponse(get_openapi(title=cfg.APP_NAME, version=cfg.API_VERSION, routes=app.routes))


@router.get("/long/{duration}")
async def long_request(duration: int):
    """Long method for testing"""
    time.sleep(duration)
    return JSONResponse([])
