import secrets

from fastapi import Request, APIRouter, HTTPException

from db import models
import utils
from db import pydantic_models as pm
import config as cfg


router = APIRouter()


@router.post("/login", response_model=pm.LoginResponse)
async def login(request: Request, data: pm.LoginRequest) -> pm.LoginResponse:
    user_obj = models.User.get_by_username(data.username)
    hashed_password = utils.password_hashing(data.password)
    if user_obj and user_obj.is_active and secrets.compare_digest(hashed_password, user_obj.password):
        access_token = utils.create_access_token(user_obj.id)
        return pm.LoginResponse(access_token=access_token, expire=cfg.JWT_AUTH_EXP_DELTA_SECONDS)
    raise HTTPException(status_code=403, detail="wrong username or password")
