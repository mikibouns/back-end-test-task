from typing import List

from fastapi import Request, APIRouter, Depends, HTTPException, Response

from db import models
from security import get_current_superuser, readonly_handler
from db import pydantic_models as pm


router = APIRouter()


@router.post("/user", response_model=pm.ResponseUser)
@readonly_handler
async def create_user(
        request: Request, data: pm.CreateUser, user: pm.User = Depends(get_current_superuser)
) -> pm.ResponseUser:
    if models.User.get_by_username(data.username):
        raise HTTPException(status_code=409, detail='object already exists')
    user_obj = models.User.create(**data.dict())
    return pm.ResponseUser(**user_obj.to_dict())


@router.get("/users", response_model=List[pm.ResponseUser])
async def get_users(request: Request, user: pm.User = Depends(get_current_superuser)) -> List[pm.ResponseUser]:
    return [pm.ResponseUser(**user_obj.to_dict()) for user_obj in models.User.all()]


@router.patch("/user/{user_id}", response_model=pm.ResponseUser)
@readonly_handler
async def update_user(
        request: Request,
        user_id: int,
        data: pm.UpdateUser,
        user: pm.User = Depends(get_current_superuser)
) -> pm.ResponseUser:
    if not models.User.get(user_id):
        raise HTTPException(status_code=404)
    user_obj = models.User.update(user_id=user_id, data=data.dict(exclude_none=True))
    return pm.ResponseUser(**user_obj.to_dict())


@router.delete("/user/{user_id}")
@readonly_handler
async def delete_user(request: Request, user_id: int, user: pm.User = Depends(get_current_superuser)):
    if not models.User.get(user_id):
        raise HTTPException(status_code=404)
    if models.User.delete(user_id=user_id):
        return Response(status_code=204)

