from fastapi.testclient import TestClient

from db import models
from db import pydantic_models as pm
import utils


# positive tests
def test_positive_create_user(client: TestClient):
    admin_obj = models.User.get_by_username('admin')
    access_token = utils.create_access_token(admin_obj.id)
    test_request_data = [
        {"username": "test_user", "is_superuser": True, "readonly": False, "password": "test_password"}
    ]
    test_response_data = [
        {
            "username": "test_user",
            "is_superuser": True,
            "is_active": True,
            "readonly": False,
            "first_name": None,
            "last_name": None
        }
    ]
    for index, data in enumerate(test_request_data):
        response = client.post("/admin/user", json=data, headers={"Authorization": access_token})
        assert response.status_code == 200
        user_obj = models.User.get_by_username('test_user')
        test_response_data[index]["id"] = user_obj.id
        assert test_response_data[index] == response.json()


def test_positive_get_users(client: TestClient):
    admin_obj = models.User.get_by_username('admin')
    access_token = utils.create_access_token(admin_obj.id)
    test_request_data = [
        {"username": "test_user", "is_superuser": True, "readonly": False, "password": "test_password"}
    ]
    test_response_data = [[pm.ResponseUser(**user_obj.to_dict()).dict() for user_obj in models.User.all()]]
    for index, data in enumerate(test_request_data):
        response = client.get("/admin/users", headers={"Authorization": access_token})
        assert response.status_code == 200
        assert test_response_data[index] == response.json()


def test_positive_update_user(client: TestClient):
    user_obj = models.User.get_by_username('test_user')
    admin_obj = models.User.get_by_username('admin')
    access_token = utils.create_access_token(admin_obj.id)
    test_request_data = [
        {"first_name": "test_first_name", "last_name": "test_last_name"}
    ]
    test_response_data = [
        {
            "username": "test_user",
            "is_superuser": True,
            "is_active": True,
            "readonly": False,
            "first_name": "test_first_name",
            "last_name": "test_last_name"
        }
    ]
    for index, data in enumerate(test_request_data):
        response = client.patch(f"/admin/user/{user_obj.id}", json=data, headers={"Authorization": access_token})
        assert response.status_code == 200
        user_obj = models.User.get_by_username('test_user')
        test_response_data[index]["id"] = user_obj.id
        assert test_response_data[index] == response.json()


def test_positive_delete_user(client: TestClient):
    user_obj = models.User.get_by_username('test_user')
    admin_obj = models.User.get_by_username('admin')
    access_token = utils.create_access_token(admin_obj.id)
    response = client.delete(f"/admin/user/{user_obj.id}", headers={"Authorization": access_token})
    assert response.status_code == 204


# negative tests
def test_negative_create_user(client: TestClient):
    admin_obj = models.User.get_by_username('admin')
    access_token = utils.create_access_token(admin_obj.id)
    test_request_data = [
        {"username": "admin", "is_superuser": True, "readonly": False, "password": "test_password"},
        {"password": "string"},
        {"username": {}, "password": "test_password"}
    ]
    test_response_data = [
        {"detail": "object already exists"},
        {"detail": [{"loc": ["body", "username"], "msg": "field required", "type": "value_error.missing"}]},
        {"detail": [{"loc": ["body", "username"], "msg": "str type expected", "type": "type_error.str"}]}
    ]
    for index, data in enumerate(test_request_data):
        response = client.post("/admin/user", json=data, headers={"Authorization": access_token})
        assert test_response_data[index] == response.json()


def test_negative_update_user(client: TestClient):
    admin_obj = models.User.get_by_username('admin')
    access_token = utils.create_access_token(admin_obj.id)
    test_request_data = [
        {"first_name": {}, "last_name": [123]}
    ]
    test_response_data = [
        {"detail": [
            {"loc": ["body", "first_name"], "msg": "str type expected", "type": "type_error.str"},
            {"loc": ["body", "last_name"], "msg": "str type expected", "type": "type_error.str"}
        ]}
    ]
    for index, data in enumerate(test_request_data):
        response = client.patch(f"/admin/user/{admin_obj.id}", json=data, headers={"Authorization": access_token})
        assert test_response_data[index] == response.json()
    response = client.patch(
        "/admin/user/666",
        json={"first_name": "test_first_name"},
        headers={"Authorization": access_token}
    )
    assert response.status_code == 404


def test_negative_delete_user(client: TestClient):
    admin_obj = models.User.get_by_username('admin')
    access_token = utils.create_access_token(admin_obj.id)
    response = client.delete(f"/admin/user/666", headers={"Authorization": access_token})
    assert response.status_code == 404
