from fastapi.testclient import TestClient


# positive tests
def test_positive_login(client: TestClient):
    test_request_data = {"username": "admin", "password": "test_password"}
    response = client.post("/auth/login", json=test_request_data)
    assert response.status_code == 200
    access_token = response.json()['access_token']
    response = client.get("/admin/users", headers={"Authorization": access_token})
    assert response.status_code == 200


# negative tests
def test_negative_login(client: TestClient):
    test_request_data = [
        {"username": "test_user_", "password": "test_password"},
        {"username": "test_user"}
    ]
    test_response_data = [
        {"detail": "wrong username or password"},
        {"detail": [{"loc": ["body", "password"], "msg": "field required", "type": "value_error.missing"}]}
    ]
    for index, data in enumerate(test_request_data):
        response = client.post("/auth/login", json=data)
        assert test_response_data[index] == response.json()


