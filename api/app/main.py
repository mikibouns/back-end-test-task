import uvicorn

from init_app import app
from routers import api, admin, auth


app.include_router(api.router, prefix="/api", tags=["API documentation"])
app.include_router(auth.router, prefix="/auth", tags=["Authorization"])
app.include_router(admin.router, prefix="/admin", tags=["Admin panel"])


if __name__ == '__main__':
    uvicorn.run(app)
