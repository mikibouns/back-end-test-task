from pydantic import BaseModel, validator

import utils


class LoginRequest(BaseModel):
    username: str
    password: str

    class Config:
        schema_extra = {"example": {
            "username": "admin",
            "password": "test_password"
        }}


class LoginResponse(BaseModel):
    access_token: str
    expire: int


class User(BaseModel):
    id: int
    is_active: bool
    is_superuser: bool
    username: str
    first_name: str = None
    last_name: str = None
    password: str


class BaseUser(BaseModel):
    username: str


class CreateUser(BaseUser):
    is_superuser: bool = False
    readonly: bool = False
    first_name: str = None
    last_name: str = None
    password: str

    @validator('password')
    def password_hashing(cls, v):
        return utils.password_hashing(v)


class ResponseUser(BaseUser):
    id: int
    readonly: bool
    is_active: bool
    is_superuser: bool
    first_name: str = None
    last_name: str = None


class UpdateUser(BaseModel):
    is_active: bool = None
    is_superuser: bool = None
    readonly: bool = None
    first_name: str = None
    last_name: str = None
    password: str = None

