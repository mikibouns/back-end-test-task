from contextlib import contextmanager

from loguru import logger
import sqlalchemy as sa

from db.init_db import Base, SessionLocal


@contextmanager
def session_scope():
    session = SessionLocal()
    try:
        yield session
    except:
        session.rollback()
        raise
    finally:
        session.close()


class User(Base):
    __tablename__ = "user"

    id = sa.Column(sa.Integer, autoincrement=True, primary_key=True, index=True)
    is_active = sa.Column(sa.Boolean, default=1)
    is_superuser = sa.Column(sa.Boolean, default=0)
    readonly = sa.Column(sa.Boolean, default=0)
    username = sa.Column(sa.String, unique=True)
    first_name = sa.Column(sa.String)
    last_name = sa.Column(sa.String)
    password = sa.Column(sa.String)

    @classmethod
    def all(cls):
        with session_scope() as session:
            user_obj_list = session.query(cls).all()
        return user_obj_list

    @classmethod
    def get(cls, user_id: int):
        with session_scope() as session:
            user_obj = session.query(cls).filter(cls.id == user_id).first()
        return user_obj

    @classmethod
    def get_by_username(cls, username: str):
        with session_scope() as session:
            user_obj = session.query(cls).filter(cls.username == username).first()
        return user_obj

    @classmethod
    def create(cls, **kwargs):
        with session_scope() as session:
            user_obj = cls(**kwargs)
            session.add(user_obj)
            session.commit()
            session.refresh(user_obj)
        return user_obj

    @classmethod
    def update(cls, user_id: int, data: dict):
        with session_scope() as session:
            session.query(cls).filter(cls.id == user_id).update(data)
            session.commit()
            user_obj = cls.get(user_id)
        return user_obj

    @classmethod
    def delete(cls, user_id: int):
        with session_scope() as session:
            state = session.query(cls).filter(cls.id == user_id).delete()
            session.commit()
        return state

    def to_dict(self):
        return dict(
            id=self.id,
            is_active=self.is_active,
            is_superuser=self.is_superuser,
            readonly=self.readonly,
            username=self.username,
            first_name=self.first_name,
            last_name=self.last_name,
            password=self.password
        )



