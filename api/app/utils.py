from datetime import datetime, timedelta
import hashlib

import jwt
from fastapi import HTTPException

import config as cfg
from db import models
from db import pydantic_models as pm


def password_hashing(password: str) -> str:
    """
    Password hashing
    :param password: password
    :return: password hash
    """
    return hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), cfg.SALT, 100000).hex()


def decode_jwt_token(access_token: str) -> int:
    """
    Decode access token
    :param access_token: access token
    :return: account ID
    """
    try:
        payload = jwt.decode(access_token, cfg.SECRET, cfg.JWT_ALGORITHM)
    except jwt.DecodeError:
        raise HTTPException(status_code=403, detail="invalid access token")
    except jwt.ExpiredSignatureError:
        raise HTTPException(status_code=403, detail="expired signature")
    else:
        return payload


def create_access_token(user_id: int) -> tuple:
    """
    Generate and get tokens
    :param user_id: user ID
    :return: access token
    """
    payload = {'user_id': user_id, 'exp': datetime.utcnow() + timedelta(seconds=cfg.JWT_AUTH_EXP_DELTA_SECONDS)}
    access_token = jwt.encode(payload, cfg.SECRET, cfg.JWT_ALGORITHM)
    return access_token


def fill_test_data() -> None:
    """
    Fill DB test data
    :return: None
    """
    user_list = [
        pm.CreateUser(username="admin", is_superuser=True, readonly=False, password="test_password"),
        pm.CreateUser(username="user1", is_superuser=True, readonly=True, password="test_password"),
        pm.CreateUser(username="user2", is_superuser=False, readonly=False, password="test_password")
    ]
    for user_data in user_list:
        if not models.User.get_by_username(user_data.username):
            models.User.create(**user_data.dict())
