#!/bin/bash
echo "RUN FASTAPI"
uvicorn main:app --workers 4 --host 0.0.0.0 --port 8080 --reload