import secrets
import functools

from fastapi import Depends, HTTPException
from fastapi.security import HTTPBasic, HTTPBasicCredentials, APIKeyHeader

import config as cfg
import utils
from db import models


security = HTTPBasic()
oauth_scheme = APIKeyHeader(name='Authorization')


def readonly_handler(func):
    @functools.wraps(func)
    async def wrapper(*args, **kwargs):
        user_obj = kwargs['user']
        if user_obj.readonly:
            raise HTTPException(status_code=403)
        return await func(*args, **kwargs)
    return wrapper


async def get_current_superuser(access_token: str = Depends(oauth_scheme)):
    """
    Get current superuser
    :param access_token: access token
    :return: user object
    """
    payload = utils.decode_jwt_token(access_token)
    user_obj = models.User.get(payload.get('user_id'))
    if user_obj and user_obj.is_superuser and user_obj.is_active:
        return user_obj
    raise HTTPException(status_code=401)


def basic_auth_validation(credentials: HTTPBasicCredentials = Depends(security)):
    correct_username = secrets.compare_digest(credentials.username, cfg.API_DOCS_USER)
    correct_password = secrets.compare_digest(credentials.password, cfg.API_DOCS_PASSWD)
    if not (correct_username and correct_password):
        raise HTTPException(
            status_code=401,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate": "Basic"},
        )
    return credentials.username
