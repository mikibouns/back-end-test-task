from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

import config as cfg
from db.models import Base
from db import init_db
import utils


Base.metadata.create_all(bind=init_db.engine)
utils.fill_test_data()


app = FastAPI(title=cfg.APP_NAME, version=cfg.API_VERSION, redoc_url='', docs_url='')
origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)


@app.on_event('startup')
async def startup_event():
    pass


@app.on_event('shutdown')
async def shutdown_event():
    pass
