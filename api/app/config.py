import os

# main config
APP_HOST = os.getenv('WEBAPP_HOST', 'localhost')
APP_PORT = int(os.getenv('WEBAPP_PORT', '8080'))
APP_NAME = os.getenv('WEBAPP_NAME', 'Test API')
API_VERSION = os.getenv('API_VERSION', '1.0.0')
API_DOCS_USER = os.getenv('API_DOCS_USER', 'admin')
API_DOCS_PASSWD = os.getenv('API_DOCS_PASSWD', 'AdminPassword')

# auth config
SECRET = os.getenv('SECRET', r'Z8vPahveGyP9vqBGxjNR6JXAMb35mt6kw3N7MxTAPCLt46WWCFp8FFVvgLn3DDnNF4HUk7TNsFf3G94n3XHmsDtmJWUqm3K9g9k4guX2LPRdsMKjaqnY2bvd7B2wdt4A')
SALT = os.getenv('SALT', b'44ef38bbea33fad08268841f2a98079569bbbe0bf46f6e36065fd70c3a31ab99')

# postgresql config
DB_HOST = os.getenv('DB_HOST', '127.0.0.1')
DB_PORT = int(os.getenv('DB_PORT', '5434'))
DB_NAME = os.getenv('DB_NAME', 'TestDB')
DB_USERNAME = os.getenv('DB_USERNAME', 'TestDB')
DB_PASSWORD = os.getenv('DB_PASSWORD', 'PkKLR2e524RmUairF4MG')

# JWT config
JWT_AUTH_EXP_DELTA_SECONDS = int(os.getenv('JWT_AUTH_EXP_DELTA_SECONDS', '3600'))
JWT_ALGORITHM = os.getenv('JWT_ALGORITHM', 'HS256')
